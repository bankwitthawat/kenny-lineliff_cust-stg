import { NextResponse } from "next/server";
import i18nConfig from "./i18n-config"
import { match as matchLocale } from "@formatjs/intl-localematcher";
import Negotiator from "negotiator";
import { i18nRouter } from 'next-i18n-router';

export function middleware(request) {
    // console.log("next middleware request :", request)
    return i18nRouter(request, i18nConfig);
  }
  
  // applies this middleware only to files in the app directory
  export const config = {
    matcher: '/((?!api|static|.*\\..*|_next).*)'
  };