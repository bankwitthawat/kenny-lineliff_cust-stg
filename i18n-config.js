const i18nConfig = {
  locales: ['en', 'th'],
  defaultLocale: 'th'
};

module.exports = i18nConfig;