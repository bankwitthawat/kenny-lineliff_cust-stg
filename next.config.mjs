import dotenv from "dotenv";
dotenv.config({ path: `.env/.env.${process.env.NEXT_PUBLIC_APP_ENV}`});

/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
};

export default nextConfig;
