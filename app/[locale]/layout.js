// "use client"

// import NavigationBar from "@/components/navbar/navigation-bar"

// export default function MainLayout({ children, params: { locale } }) {
//     console.log("MainLayout", locale)
//     return (
//         <>
//             <NavigationBar />
//             <div className="flex flex-col mt-20 h-full w-full">
//                 {children}
//             </div>
//         </>
//     )
// }


import { Inter } from "next/font/google";
import "../globals.css";
import { LiffProvider } from '../providers/LiffProvider'
import i18nConfig from "../../i18n-config"
import { dir } from 'i18next';
import NavigationBar from "@/components/navbar/navigation-bar"
import TranslationsProvider from '@/app/providers/TranslationProvider';
import initTranslations from '../i18n';
import { cn } from "@/lib/utils"

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
    title: "KENNY Services",
    description: "",
};

export async function generateStaticParams() {
    return i18nConfig.locales.map((locale) => ({ locale }));
}


export default async function RootLayout({ children, params: { locale } }) {
    const navi18n = ['navbar'];
    const { resources } = await initTranslations(locale, navi18n);

    return (
        <LiffProvider liffId={process.env.NEXT_PUBLIC_LIFF_ID || ''}>
            <html lang={locale}>
                <body>
                    <div className="flex flex-col">
                        <TranslationsProvider
                            namespaces={navi18n}
                            locale={locale}
                            resources={resources}>
                            <NavigationBar />
                        </TranslationsProvider>
                        <main className="my-20">{children}</main>
                    </div>
                </body>
            </html>
        </LiffProvider>
    );
}
