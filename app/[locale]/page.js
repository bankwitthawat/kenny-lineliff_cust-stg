import initTranslations from '../i18n';

import ServiceCard from "../(components)/service-card"


const services = [
    {
        code: "1",
        name: "ทำความสะอาดทั่วไป คอนโด / บ้าน",
        subServices: [
            {
                code: "1-1",
                name: "ทำความสะอาดทั่วไป 2 ชั่วโมง",
                price: 600
            },
            {
                code: "1-2",
                name: "ทำความสะอาดทั่วไป 3 ชั่วโมง",
                price: 600
            },
            {
                code: "1-3",
                name: "ทำความสะอาดทั่วไป 4 ชั่วโมง",
                price: 600
            },
            {
                code: "1-3",
                name: "ทำความสะอาดทั่วไป 8 ชั่วโมง",
                price: 600
            },
        ]
    },
    {
        code: "2",
        name: "ทำความสะอาดใหญ่ (Big Cleaning)",
        subServices: [
            {
                code: "2-1",
                name: "Big Cleaning 1",
                price: 600
            },
            {
                code: "2-2",
                name: "ทำความสะอาดทั่วไป 4 ชั่วโมง",
                price: 600
            },
            {
                code: "2-3",
                name: "ทำความสะอาดทั่วไป 6 ชั่วโมง",
                price: 600
            },
        ]
    },
    {
        code: "3",
        name: "บริการซัก โซฟา / ม่าน / พรม",
        subServices: [
            {
                code: "3-1",
                name: "ซักโซฟาและที่นอน",
                price: 2500
            },
            {
                code: "3-2",
                name: "ทำความสะอาดผ้าม่าน",
                price: 1500
            },
            {
                code: "3-3",
                name: "ทำความสะอาดพรม",
                price: 1500
            },
            {
                code: "3-5",
                name: "กำจัดไรฝุ่นเตียง",
                price: 1000
            },
            {
                code: "3-5",
                name: "ดูดไรฝุ่นโซฟา",
                price: 1000
            },
        ]
    },
];

export default async function Home({ params: { locale } }) {

    const { t } = await initTranslations(locale, ['navbar']);

    return (
        <div className='flex flex-col w-full'>
            {/* {t('home')} */}

            {services.map((service, index) => (
                <ServiceCard key={service.code} service={service} />
            ))}


        </div>
    );
}