import { Resource, createInstance, i18n } from 'i18next';
import { initReactI18next } from 'react-i18next/initReactI18next';
import resourcesToBackend from 'i18next-resources-to-backend';
import i18nConfig from '@/i18n-config';

export default async function initTranslations(locale, namespaces, i18nInstance, resources) {
    i18nInstance = i18nInstance || createInstance();
    i18nInstance.use(initReactI18next);

    if (!resources) {
        i18nInstance.use(
            resourcesToBackend(
                (language, namespace) =>
                    import(`@/locales/${language}/${namespace}.json`)
            )
        );
    }

    await i18nInstance.init({
        lng: locale,
        resources,
        fallbackLng: i18nConfig.defaultLocale,
        supportedLngs: i18nConfig.locales,
        defaultNS: namespaces[0],
        fallbackNS: namespaces[0],
        ns: namespaces,
        preload: resources ? [] : i18nConfig.locales,

        // react-i18next options
        // react: {
        //     wait: true
        // }
    });

    return {
        i18n: i18nInstance,
        resources: i18nInstance.services.resourceStore.data,
        t: i18nInstance.t
    };

}

// import i18n from "i18next";
// import detector from "i18next-browser-languagedetector";
// import { reactI18nextModule } from "react-i18next";

// import translationEN from '../locales';

// // the translations
// const resources = {
//     en: {
//         translation: translationEN
//     },
//     th: {
//         translation: translationTH
//     }
// };

// i18n
//     .use(detector)
//     .use(reactI18nextModule) // passes i18n down to react-i18next
//     .init({
//         resources,
//         fallbackLng: "th", // use en if detected lng is not available

//         keySeparator: false, // we do not use keys in form messages.welcome

//         interpolation: {
//             escapeValue: false // react already safes from xss
//         }
//     });

// export default i18n;