import { create } from "zustand"

export const useDialog = create((set) => ({
    isOpen: false,
    onOpen: () => set({ isOpen: true }),
    onClose: () => set({ isOpen: true }),
    data: {},
    setData: (data) => set({ data: { data } })
}))