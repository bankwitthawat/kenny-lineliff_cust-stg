"use client"

import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card"
import {
    Carousel,
    CarouselContent,
    CarouselItem,
    CarouselNext,
    CarouselPrevious,
} from "@/components/ui/carousel"
import { cn } from "@/lib/utils"
import { NumericFormat } from 'react-number-format';

export default function ServiceCard(props) {

    // console.log("ServiceCard", props);

    return (
        <div className='flex flex-col gap-y-4 p-4 md:px-60 mt-4 md:mt-10 '>
            <div className='text-xl'>{props.service.name}</div>
            <Carousel
                opts={{
                    align: "start",
                }}
                className="w-full"
            >
                <CarouselContent>
                    {props.service.subServices.map((s, index) => (
                        <CarouselItem key={index} className="basis-1/2 md:basis-1/3 lg:basis-1/4">
                            <div className="p-1">
                                <Card>
                                    <CardContent className="flex aspect-video items-center justify-center p-6">
                                        <span className="text-3xl font-semibold">{index + 1}</span>
                                    </CardContent>
                                </Card>
                                <div className="flex flex-col mt-4">
                                    <div className="text-md font-semibold text-green-600">{s.name}</div>
                                    <div className="flex flex-row w-full text-sm">
                                        <span className="">เริ่มต้น ฿</span>
                                        <NumericFormat value={s.price} thousandSeparator="," />
                                    </div>
                                </div>
                            </div>
                        </CarouselItem>
                    ))}
                </CarouselContent>
                <CarouselPrevious />
                <CarouselNext />
            </Carousel>
        </div>
    );
}