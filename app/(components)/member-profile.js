"use client"

import React, { useState } from 'react';
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuLabel,
    DropdownMenuSeparator,
    DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu"
import {
    Avatar,
    AvatarFallback,
    AvatarImage,
} from "@/components/ui/avatar"
import { Button } from "@/components/ui/button"
import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogFooter,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from "@/components/ui/dialog"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"

import { useTranslation } from 'react-i18next';


export default function Profile() {

    const [isVerifyDialogOpen, setIsVerifyDialogOpen] = useState(false);
    const [isOTPDialogOpen, setIsOTPDialogOpen] = useState(false);

    const { t } = useTranslation();

    const openDialog = () => {
        console.log("openDialog");
        setIsVerifyDialogOpen(true);
    };

    const closeDialog = () => {
        setIsVerifyDialogOpen(false);
    };

    return (
        <>
            <div className="flex flex-row items-center gap-x-2 cursor-pointer" onClick={openDialog}>
                <Avatar className="h-7 w-7">
                    <AvatarImage alt="User Avatar" src="/placeholder-avatar.jpg" />
                    <AvatarFallback className="bg-gray-200 dark:bg-gray-700 text-gray-800 dark:text-gray-400 flex items-center justify-center">
                        <UserIcon className="h-5 w-5" />
                    </AvatarFallback>
                </Avatar>
                <span className="text-sm">{t('newMember')}</span>
            </div>
            <VerifyMemberDialog open={isVerifyDialogOpen} onOpenChange={setIsVerifyDialogOpen} />
        </>
    );
}

function VerifyMemberDialog(props) {
    console.log("VerifyMemberDialog", props)
    const { t } = useTranslation();

    const submitHandler = (e) => {
        e.preventDefault();
        console.log("submitHandler");

    }

    return (
        <Dialog open={props.open} onOpenChange={props.onOpenChange}>
            {/* <DialogTrigger asChild>
                <div className="flex flex-row items-center gap-x-2 cursor-pointer">
                    <Avatar className="h-7 w-7">
                        <AvatarImage alt="User Avatar" src="/placeholder-avatar.jpg" />
                        <AvatarFallback className="bg-gray-200 dark:bg-gray-700 text-gray-800 dark:text-gray-400 flex items-center justify-center">
                            <UserIcon className="h-5 w-5" />
                        </AvatarFallback>
                    </Avatar>
                    <span className="text-sm">{t('newMember')}</span>
                </div>
            </DialogTrigger> */}
            <DialogContent className="sm:max-w-[425px]">
                <DialogHeader>
                    <DialogTitle>ยืนยันตัวตน</DialogTitle>
                    <DialogDescription>
                        โปรดระบุข้อมูลเพื่อยืนยันตัวตนในการเข้ารับบริการ
                    </DialogDescription>
                </DialogHeader>
                <div className="flex flex-col gap-4 py-4">
                    <div className="grid w-full max-w-sm items-center gap-1.5">
                        <Label htmlFor="prefixName">คำนำหน้า</Label>
                        <Input type="text" id="prefixName" placeholder="" />
                    </div>
                    <div className="grid w-full max-w-sm items-center gap-1.5">
                        <Label htmlFor="firstName">ชื่อ</Label>
                        <Input type="text" id="firstName" placeholder="" />
                    </div>
                    <div className="grid w-full max-w-sm items-center gap-1.5">
                        <Label htmlFor="lastName">นามสกุล</Label>
                        <Input type="text" id="lastName" placeholder="" />
                    </div>
                    <div className="grid w-full max-w-sm items-center gap-1.5">
                        <Label htmlFor="mobileNo">เบอร์โทรศัพท์มือถือ</Label>
                        <Input type="text" id="mobileNo" placeholder="" />
                    </div>
                    <div className="grid w-full max-w-sm items-center gap-1.5">
                        <Label htmlFor="email">อีเมล</Label>
                        <Input type="email" id="email" placeholder="" />
                    </div>
                </div>
                <DialogFooter>
                    <Button type="submit" onClick={submitHandler}>ยืนยัน</Button>
                </DialogFooter>
            </DialogContent>
        </Dialog>
    );
}

function VerifyMemberOTPDialog() {
    return (
        <>
            <Dialog open={isOTPDialogOpen} onOpenChange={setIsOTPDialogOpen}>
                <DialogContent className="sm:max-w-[425px]">
                    <DialogHeader>
                        <DialogTitle>OTP Verification</DialogTitle>
                        <DialogDescription>
                            โปรดระบุข้อมูลเพื่อยืนยันตัวตนในการเข้ารับบริการ
                        </DialogDescription>
                    </DialogHeader>
                    <div className="flex flex-col gap-4 py-4">
                        <div className="grid w-full max-w-sm items-center gap-1.5">
                            <Label htmlFor="prefixName">คำนำหน้า</Label>
                            <Input type="text" id="prefixName" placeholder="" />
                        </div>
                        <div className="grid w-full max-w-sm items-center gap-1.5">
                            <Label htmlFor="firstName">ชื่อ</Label>
                            <Input type="text" id="firstName" placeholder="" />
                        </div>
                        <div className="grid w-full max-w-sm items-center gap-1.5">
                            <Label htmlFor="lastName">นามสกุล</Label>
                            <Input type="text" id="lastName" placeholder="" />
                        </div>
                        <div className="grid w-full max-w-sm items-center gap-1.5">
                            <Label htmlFor="mobileNo">เบอร์โทรศัพท์มือถือ</Label>
                            <Input type="text" id="mobileNo" placeholder="" />
                        </div>
                        <div className="grid w-full max-w-sm items-center gap-1.5">
                            <Label htmlFor="email">อีเมล</Label>
                            <Input type="email" id="email" placeholder="" />
                        </div>
                    </div>
                    <DialogFooter>
                        <Button type="submit">ยืนยัน</Button>
                    </DialogFooter>
                </DialogContent>
            </Dialog>
        </>
    );
}

function UserIcon(props) {
    return (
        <svg
            {...props}
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M19 21v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2" />
            <circle cx="12" cy="7" r="4" />
        </svg>
    )
}