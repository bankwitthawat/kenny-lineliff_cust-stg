'use client';

import React, {
  createContext,
  FC,
  PropsWithChildren,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
// import { Liff } from '@line/liff';

const LiffContext = createContext({ liff: null, liffError: null });
export const useLiffContext = () => useContext(LiffContext);

export const LiffProvider = ({ children, liffId }) => {
    const [liff, setLiff] = useState(null);
    const [liffError, setLiffError] = useState(null);
  
    const initLiff = useCallback(async () => {
      try {
        const liffModule = await import('@line/liff');
        const liff = liffModule.default;
        console.log('LIFF init...');
  
        await liff.init({ liffId });
  
        console.log('LIFF init succeeded.');
        setLiff(liff);

        if (!liff.isLoggedIn()) {
          liff.login();
          return false;
        }


      } catch (error) {
        console.log('LIFF init failed.');
        setLiffError((error).toString());
      }
    }, [liffId]);
  
    // init Liff
    useEffect(() => {
      console.log('LIFF init start...');
      initLiff();
    }, [initLiff]);
  
    return (
      <LiffContext.Provider value={{ liff, liffError }}>
        {children}
      </LiffContext.Provider>
    );
  };